# ---------------------------------------------------------------------------------------------------------------------
# TERRAGRUNT CONFIGURATION
# This is the configuration for Terragrunt, a thin wrapper for Terraform that supports locking and enforces best
# ---------------------------------------------------------------------------------------------------------------------

terragrunt = {
  # Terragrunt will copy the Terraform configurations specified by the source parameter, along with any files in the
  # working directory into a temporary folder, and execute your Terraform commands in that folder.
  terraform {
    source = "bitbucket.org/ascit/tf-aws-infrastructure.git//modules//vpc///vpc?ref=v0.0.1"
  }

  # Include all settings from the root terraform.tfvars file
  include = {
    path = "${find_in_parent_folders()}"
  }
}

# ---------------------------------------------------------------------------------------------------------------------
# MODULE PARAMETERS
# These are the variables we have to pass in to use the module specified in the terragrunt configuration above
# ---------------------------------------------------------------------------------------------------------------------

key_name            =
web_instance_type   =
web_count           =
web_subnet          =
web_security_group  =
app_instance_type   =
app_count           =
app_subnet          =
app_security_group  =
environment         =
  