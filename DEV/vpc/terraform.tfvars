# ---------------------------------------------------------------------------------------------------------------------
# TERRAGRUNT CONFIGURATION
# This is the configuration for Terragrunt, a thin wrapper for Terraform that supports locking and enforces best
# ---------------------------------------------------------------------------------------------------------------------
  # Terragrunt will copy the Terraform configurations specified by the source parameter, along with any files in the
  # working directory into a temporary folder, and execute your Terraform commands in that folder.
terragrunt = {
  terraform {
    source = "bitbucket.org/ascit/tf-aws-infrastructure.git//modules//vpc?ref=v0.0.13"
  }
  include = {
    path = "${find_in_parent_folders()}"
  }
}

# ---------------------------------------------------------------------------------------------------------------------
# MODULE PARAMETERS
# These are the variables we have to pass in to use the module specified in the terragrunt configuration above
# ---------------------------------------------------------------------------------------------------------------------

key_name        = "london-pair"
environment     = "dev"
cidr            = "10.220.32.0/21"
public1         = "10.220.32.0/24"
public2         = "10.220.33.0/24"
private1        = "10.220.34.0/24"
private2        = "10.220.35.0/24"
region          = "eu-west-2"
name            = "gps-dev"
gatewayname     = "dev-igw"
core_cidr       = "10.220.0.0/21"
private1_core   = "10.220.1.0/24"
private2_core   = "10.220.3.0/24"
public1_core    = "10.220.0.0/24"
public2_core    = "10.220.2.0/24"
