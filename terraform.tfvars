terragrunt = {
  # Configure Terragrunt to automatically store tfstate files in an S3 bucket
  remote_state {
    backend = "s3"
    config {
      encrypt        = true
      bucket         = "gps-aws-tfstate"
      key            = "${path_relative_to_include()}/terraform.tfstate"            # The Interpolation function "path_relative_to_include" is used to ensure that each child stores state file at a different key.
      region         = "eu-west-2"
      dynamodb_table = "devgps-tf-locks"
    }
  }
}

# Terragrunt is a thin wrapper for Terraform that provides extra tools for working with multiple Terraform modules